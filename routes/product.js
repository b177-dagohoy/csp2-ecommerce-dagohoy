const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");
const { trusted } = require("mongoose");

router.post("/", auth.verify, (req, res) => {
    const data = {
        product : req.body,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }

    productController.createProduct(data).then(resultFromController => res.send(resultFromController))
})

router.get("/", (req, res) => {
    productController.getActiveTreats().then(resultFromController => res.send(resultFromController))
})

router.get("/:productId", (req, res) => {
    productController.getTreats(req.params).then(resultFromController => res.send(resultFromController))
})

router.put("/:productId", auth.verify, (req, res) =>{
    const update = {
        product : req.body,
        productId : req.params.productId,
        isAdmin : auth.decode(req.headers.authorization).isAdmin    
    }
    productController.updateProduct(update).then(resultFromController => res.send(resultFromController))
})

router.put("/:productId/archive", auth.verify, (req, res) => {
    const archives = {
        productId : req.params.productId,
        isAdmin : auth.decode(req.headers.authorization).isAdmin    
    }
    productController.archiveProduct(archives).then(resultFromController => res.send(resultFromController));
    
});

module.exports = router;
