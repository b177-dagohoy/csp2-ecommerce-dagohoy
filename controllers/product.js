const Product = require("../models/product")
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.createProduct = (data) => {
    if(data.isAdmin){
        let newProduct = new Product({
            name : data.product.name,
            expiration : data.product.expiration,
            description : data.product.description,
            price : data.product.price,
            stock : data.product.stock
        })

        return newProduct.save().then((product, error) =>{
            if(error){
                return false;
            }
            else{
                return true;
            }
        })
    }
    else{
        return Promise.resolve("Request Denied. Please contact customer support.");
    }
}

module.exports.getActiveTreats = () => {
    return Product.find({isActive : true}).then(result => {
        return result
    })
}

module.exports.getTreats = (reqParams) => {
    return Product.findById(reqParams.productId).then(result => {
        return result
    })
}

module.exports.updateProduct = (update) => {
    if(update.isAdmin){
        let updatedProduct = {
            name : update.product.name,
            description : update.product.description,
            price : update.product.price,
            stock: update.product.stock
        }

        return Product.findByIdAndUpdate(update.productId, updatedProduct).then((product, error) => {
            if(error){
                return false
            }
            else{
                return true
            }
        })
    }
    else{
        return Promise.resolve("Request Denied. Please contact customer support.");
    }
}

module.exports.archiveProduct = (archives) => {
    if (archives.isAdmin) {
        let updateActiveField = {
            isActive : false
        };

        return Product.findByIdAndUpdate(archives.productId, updateActiveField).then((product, error) => {
            if (error) {
                return false;
            } 
            else {

                return true;

            }

        })
    } 
    else {
        return Promise.resolve("Access denied. Please contact your administrator.");
    }

}
