const User = require("../models/user")
const Product = require("../models/product")
const Order = require("../models/order")
const bcrypt = require("bcrypt")
const auth = require("../auth")

module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        fullName : reqBody.fullName,
        email : reqBody.email,
        mobileNo : reqBody.mobileNo,
        password : bcrypt.hashSync(reqBody.password, 10)
    })

    return newUser.save().then((user, error) => {
        if(error){
            return false;
        }
        else{
            return true;
        }
    })
}

module.exports.loginUser = (reqBody) => {
    return User.findOne({email : reqBody.email}).then(result => {
        if(result == null){
            return false;
        }
        else{
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

            if(isPasswordCorrect){
                return { access : auth.createAccessToken(result)}
            }
            else{
                return false;
            }
        }
    })
}

module.exports.setAdmin = (data) => {

    if (data.isAdmin) {
        let updateAdminField = {
            isAdmin : true
        };

        return User.findByIdAndUpdate(data.userId, updateAdminField).then((user, error) => {
            if (error) {
                return false;
            } 
            else {

                return true;

            }

        })
    } 
    else {
        return Promise.resolve("Request Denied. Please contact customer support.");
    }

}

module.exports.createOrder = async(data) => {
    if(data.isAdmin == false) {
        let userOrder = await Product.findById(data.productId).then(product =>{
            if(product.isActive == true){
                product.updateOne({$set : {stock : product.stock - data.quantity}}).then(product => {})

                let newOrder = new Order({
                    userId: data.userId,
                    productId: data.productId,
                    purchasedOn: new Date(),
                    quantity: data.quantity,
                    totalAmount: product.price * data.quantity,
                    status: "New"
                })

                return newOrder.save().then((product, error) => {
                    if(error){
                        return false
                    }
                    else {
                        return true
                    }
                })
            }
            else {
                return false
            }
        })

        let isProductUpdated = await Product.findById(data.productId).then(product =>{
            if(product.isActive == true){
                product.purchaseOrder.push({userId : data.userId})

                return product.save().then((product, error) => {
                    if(error){
                        return false;
                    }
                    else{
                        return true;
                    }
                })
            }
            else {
                return false
            }
        })

        let isUserCartUpdated = await User.findById(data.userId).then(user =>{
            user.userCart.push({productId : data.productId})
 
            return user.save().then((user, error) => {
                 if(error){
                     return false;
                 }
                 else{
                     return true;
                 }
             })
         })

         if(
            userOrder &&
            isProductUpdated &&
            isUserCartUpdated
         ){
            return "Order Successfully Placed"
         }
         else {
            return "Cannot process order at this time"
         }
    }
    else{
        return Promise.resolve("Order cannot be processed at this time. Admin user detected.");
    }
}

module.exports.getAllOrders = (data) => {
    if(data.isAdmin) {
        return Order.find({}).then(result => {
            return result
        })
    }
    else{
        return Promise.resolve("Request Denied. Please contact customer support.");
    }
}

module.exports.getMyOrders = (data) => {
    return Order.find({userId : data.userId}).then(order => {
        if(data.isAdmin == false){
            return order
        }
        else {
            return "Access denied"
        }
    })
}
