const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");

const app = express();

mongoose.connect(
    "mongodb+srv://admin:9HhP3KqCrVnPDb42@wdc028-course-booking.eerle.mongodb.net/csp2-ecommerce?retryWrites=true&w=majority",{
        useNewUrlParser: true,
        useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log("Connected to MongoDB"))

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/user/", userRoutes);
app.use("/product/", productRoutes);

app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${process.env.PORT || 4000}`)
});